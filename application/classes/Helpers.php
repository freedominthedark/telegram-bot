<?php

namespace application\classes;

use Exception;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Helpers\Emojify;

class Helpers{

    public function __construct(){}
    
    public function __destruct(){}

    public static function sendMessage($arr, $chat_id, $telegram, $reply)
    {
        $telegram->sendMessage([ 'chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML', 'reply_markup' => Keyboard::make(['inline_keyboard' => [], 'resize_keyboard' => false, 'one_time_keyboard' => true ])]);
    }

}
<?php
namespace application\classes;

class configBot {
    # BASE
    public static $HostDB = "localhost";
    public static $UserDB = "root";
    public static $PassDB = "";
    public static $BaseDB = "base";
    public static $charset = "utf8mb4";

    # BOT TOKEN
    public static $botToken = '5**3:AA**K8';
    public $botTokens = '5**3:AA**K8';

    # Для запуска открыть в бараузере сылку
    # https://api.telegram.org/bot2**4:AA**TI/setWebhook?url=https://sait.ru/bot.php
    # Для того что бы узнать ID канала
    # https://api.telegram.org/bot2**4:AA**TI/sendMessage?chat_id=@TestBot&text=123test
    # или Самый простой способ - пригласить @get_id_bot в свой чат и набрать: /my_id@get_id_bot Внутри вашего чата

    # Админ ID (chat_id), получить здесь @userinfobot
    public $idAdmin = 123445678;

    public $channelId = "-1001234567890";

    # URL сайта
    public static $urlSite = "https://sait.ru/bot.php";

    public $folderName = "/";
}

<?php
namespace application\classes;

class Log{

    /**
     * Лог ошибок SQL
     * @param  [type] $typelog  [description]
     * @param  [type] $log_text [description]
     * @return [type]           [description]
     */
    public function writelog($typelog, $log_text, $message) {

        $error = $log_text;
        
        if (isset($error[1]['args'][1][0])) {
            $arg = $error[1]['args'][1][0];
        }else{
            $arg = "";
        }

        $errorForLog = date("y.m.d H:m:s")."\t
            Message: ".$message."

            File: ".$error[0]['file']."
            Lile: ".$error[0]['line']."
            Function: ".$error[0]['function']."
            Class: ".$error[0]['class']."
            Type: ".$error[0]['type']."
            =======Where?======\r\n
            File: ".$error[1]['file']."
            Lile: ".$error[1]['line']."
            Function: ".$error[1]['function']."
            Class: ".$error[1]['class']."
            Type: ".$error[1]['type']."
            Args: ".$error[1]['args'][0]."
            Args: ".$arg."
            ";

        define('ROOT', $_SERVER['DOCUMENT_ROOT']);
        file_put_contents(ROOT.'/log/sql_error.txt', "$errorForLog\r\n", FILE_APPEND);
    }

}
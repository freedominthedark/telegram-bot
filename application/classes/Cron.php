<?php
namespace application\classes;

/**
 * CRON 
 *
 * PHP version 7.0
 */
class Cron {

    private $b;

    public function __construct()
    {
        $this->b = new Bot();
    }

    public function offPrila($id = 0)
    {
        # смотрим всех со статусом 6, тех кото выключать
        foreach ($this->b->getAllGroupPrila(6) as $k => $v) {
            # ставим статус Off
            $this->b->setOneGroupPrila("status", 666, $v['group_id'], $v['prila_id']);
        }
    }

}

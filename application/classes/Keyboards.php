<?php
namespace application\classes;

use Telegram\Bot\Api; 
use Telegram\Bot\Helpers\Emojify;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class Keyboards {

    public static function actions(): array{
        return [
            /*'keyboard_main' => [
                [
                    Keyboard::inlineButton(['callback_data'=>'0','text'=>'📱 Админка']),
                ]
            ],*/
            'keyboard_cancel' => [
                [
                    Keyboard::button(['text'=>'❌ Отмена']),
                ]
            ],
            'keyboard_opros_next' => [
                [
                    Keyboard::button(['text'=>'ДАЛЬШЕ']),
                    Keyboard::button(['text'=>'🔙 Назад']),
                ],[
                    Keyboard::button(['text'=>'❌ Отмена']),
                ]
            ],
            'keyboard_opros' => [
                [
                    Keyboard::button(['text'=>'НАЧАТЬ']),
                    Keyboard::button(['text'=>'📞 Контакты']),
                ]
            ],
            'keyboard_main' => [
                [
                    //Keyboard::button(['text'=>'Текст => Фото']),
                    Keyboard::button(['text'=>'❌ Отмена']),
                ]
            ],
            'back' => [
                [
                    Keyboard::button(['text'=>'🔙 Назад']),
                    Keyboard::button(['text'=>'❌ Отмена']),
                ]
            ],
            'oneback' => [
                [
                    Keyboard::button(['text'=>'🔙 Назад']),
                ]
            ],
            'keyboard_main_admin' => [
                [
                    Keyboard::button(['text'=>'📱 Админка']),
                ]
            ],
            'keyboard_admin' => [
                [
                    Keyboard::button(['text'=>'🙊️ Замьюченые']),
                    Keyboard::button(['text'=>'🙍🏻‍♂️ Все пользователи']),
                ],[
                    Keyboard::button(['text'=>'📱 Админка']),
                    Keyboard::button(['text'=>'⚙️ Настройки']),
                ]
            ],
            'cancel' => [
                [Keyboard::inlineButton(['callback_data'=>'cancel','text'=> Emojify::text(':x:').' Отмена']),],
            ],
        ];
    }

}
?>
<?php

namespace application\classes;

use Telegram\Bot\Keyboard\Keyboard;

class Bot{

    private $db;
    private $time;
    private $conf;

    public function __construct(){
        $this->db = Database::getInstance();
        $this->time = time();
        $this->conf = new configBot();
    }
    
    public function __destruct(){}

    public function format($number, $key = 2)
    {
        return number_format($number, $key, '.', '');
    }

    public function checkUser($chat_id, $telegram, $first_name, $last_name, $username)
    {
        # Добавим в базу пользователя если еще нет
        $row_users = $this->db->getColumn("SELECT COUNT(*) FROM `db_users_bot4` WHERE `chat_id` = ?" , [$chat_id]);
        if($row_users == 0){
            $this->setNewUserBase($first_name, $last_name, $username, $chat_id);

            # Отправим в бот
            $reply = "+1 " . $first_name . " ". $last_name . " ". $username. " chat_id: " . $chat_id;

            $telegram->sendMessage([ 'chat_id' => $this->conf->idAdmin, 'text' => $reply, 'parse_mode' => 'HTML', 'reply_markup' => Keyboard::make(['inline_keyboard' => [], 'resize_keyboard' => false, 'one_time_keyboard' => true ])
            ]);
            # END
        }
    }

    public function categoriesToTree(&$categories) {

        $map = array(
            0 => array('subcategories' => array())
        );

        foreach ($categories as &$category) {
            $category['subcategories'] = array();
            $map[$category['id']] = &$category;
        }

        foreach ($categories as &$category) {
            $map[$category['parent_id']]['subcategories'][] = &$category;
        }

        return $map[0]['subcategories'];

    }

    public function getSettings()
    {
        $data = $this->db->getRow("SELECT * FROM db_settings WHERE id = ? LIMIT ?" , [1, 1]);
        return $data;
    }

    public function updateSettings($val)
    {
        $this->db->updateRow("UPDATE `db_settings` SET on_off = ? WHERE id = ?",[$val, 1]);
    }

    public function updateSettingsVal($val, $key)
    {
        return $this->db->updateRow("UPDATE `db_settings` SET `".$key."` = ? WHERE id = ?",[$val, 1]);
    }

    public function isAdmin($chat_id)
    {
        # Если не Админ, то выходим
        if ($chat_id == $this->conf->idAdmin) {
            return true;
        }else return false;
    }

    public function getUserInfo($id)
    {
        $data = $this->db->getRow("SELECT * FROM db_users_bot4 WHERE chat_id = ?" , [$id]);
        return $data;
    }

    public function getUserInfoOne($id, $val)
    {
        $data = $this->db->getRow("SELECT `".$val."` FROM db_users_bot4 WHERE chat_id = ?" , [$id]);
        return $data[$val];
    }

    public function setOneUsersDb($key, $val, $chat_id)
    {
        return $this->db->updateRow("UPDATE `db_users_bot4` SET ".$key." = ? WHERE chat_id = ?",[$val, $chat_id]);
    }

    public function getAllUsers()
    {
        $data = $this->db->getRows("SELECT * FROM db_users_bot4 ORDER BY id DESC LIMIT 25", []);
        $res = [];
        foreach($data as $row){
            $res[] = $row;
        }
        return $res;
    }

    public function Auth($chat_id)
    {
        $time = time();

        if ($this->isAdmin($chat_id) == true) {

            $query = "INSERT INTO db_auths (date_add, key_auth) VALUES (?,?)";

            $salt = 'DemiMur';
            $key = $this->conf->idAdmin.''.$salt.''.$time;
            $key_auth = hash('sha256', $key);

            $this->db->insertRow($query, [$time, $key_auth]);

            return $key_auth;

        }else return false;
    }

    public function isBan($chat_id)
    {
        # Если не Админ, то выходим
        if ($this->getUserInfo($chat_id)['ban'] == 1) {
            return true;
        }else return false;
    }

    public function getStrripos($name)
    {
        if (is_string($name)) {
            return true;
        }else{

            $haystack = $name;
            $needle   = '\u';

            $pos      = strripos($haystack, $needle);

            if ($pos === false) {
                #echo "К сожалению, ($needle) не найдена в ($haystack)";
                return true;
            } else {
                #echo "Поздравляем!\n";
                #echo "Последнее вхождение ($needle) найдено в ($haystack) в позиции ($pos)";
                return false;
            }
        }
    }

    public function setNumStr($str)
    {
        preg_match("~([0-9])+$~", $str, $matches);
        return $matches[0];
    }

    /**
     * Добавление в базу нового пользователя
     * @param [type] $login   Имя/логин пользователя
     * @param [type] $chat_id ID чата/ID юзера
     */
    public function setNewUserBase($first_name, $last_name, $username, $chat_id)
    {
        $time = time();

        $query = "INSERT INTO db_users_bot4 (chat_id, first_name, last_name, username, date_reg) VALUES (?,?,?,?,?)";
        $this->db->insertRow($query, [$chat_id,$first_name,$last_name,$username,$time]);

        return $user_id = $this->db->getlastInsertId();
    }

    public function translitURL($str){
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ё"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", 
            " -"=> "", ","=> "", " "=> "-", "."=> "", "/"=> "_", 
            "-"=> ""
        );
        return strtr($str,$tr);
    }

    public function dellStep($chat_id)
    {
        $this->db->deleteRow("DELETE FROM db_step WHERE chat_id = ?",[$chat_id]);
    }

    public function dellStepWhere($chat_id, $where)
    {
        $this->db->deleteRow("DELETE FROM db_step WHERE chat_id = ? AND type = ?",[$chat_id, $where]);
    }

    public function getText($id)
    {
        $data = $this->db->getRow("SELECT * FROM db_text WHERE id = ?" , [$id]);
        return $data;
    }

    public function warningBan($chat_id)
    {
        if ($this->getUserInfo($chat_id)['warning'] == 3) {
            # БАНим
            $this->db->updateRow("UPDATE `db_users_bot4` SET ban = ? WHERE chat_id = ?",[1, $chat_id]);
            return true;
        }else{
            $this->db->updateRow("UPDATE `db_users_bot4` SET warning = warning + ? WHERE chat_id = ?",[1, $chat_id]);
            return false;
        }
    }

    /**
     * Обновление баланса любого
     * Пример: takeSum($chat_id, 'BTC', $sum, $oper = false)
     * @param  [type]  $chat_id [description]
     * @param  [type]  $type    [description]
     * @param  [type]  $sum     [description]
     * @param  boolean $oper    [description]
     * @return [type]           [description]
     */
    public function takeSum($chat_id, $type, $sum, $oper = false)
    {
        if ($oper === true){
            $oper = '+';
        }else $oper = '-';

        #$this->db->updateRow("UPDATE `db_users_bot4` SET '.$type.' = '.$type.' '.$oper.' ? WHERE chat_id = ?",[$sum, $chat_id]);
        $this->db->updateRow("UPDATE `db_users_bot4` SET ".$type." = ".$type." ".$oper." ? WHERE chat_id = ?",[$sum, $chat_id]);
    }

    public function setMember($chat_id)
    {
        $this->db->updateRow("UPDATE `db_users_bot4` SET member = ? WHERE chat_id = ?",[1, $chat_id]);
    }

    public function setLang($lang, $chat_id)
    {
        $this->db->updateRow("UPDATE `db_users_bot4` SET lang = ? WHERE chat_id = ?",[$lang, $chat_id]);
    }

    public function setStep($chat_id, $type, $name, $status)
    {
        $query = "INSERT INTO db_step (chat_id, type, name_step, status) VALUES (?,?,?,?)";

        $this->db->insertRow($query, [$chat_id, $type, $name,$status]);

        return $id = $this->db->getlastInsertId();
    }

    public function getStep($chat_id)
    {
        $data = $this->db->getRow("SELECT * FROM db_step WHERE chat_id = ? ORDER BY id DESC LIMIT ?" , [$chat_id, 1]);
        return $data;
    }

    public function getStepWhere($chat_id, $type)
    {
        $data = $this->db->getRow("SELECT * FROM db_step WHERE chat_id = ? AND type = ? ORDER BY id DESC LIMIT ?" , [$chat_id, $type, 1]);
        return $data;
    }

    /*======================================================================*\
    Function:   numPercent
    Descriiption: Процент от числа
    $percent - Процент в процентах
    $num - Число от которого ищем процент
    $numPercent = $func->numPercent ( $dayPercent , $table['sum']);
    \*======================================================================*/
    public function numPercent ($percent, $num) {
        //Чтобы найти процент от числа, например, 35% от 1000 рублей, нужно
        //1000*35:100=350 рублей
        $numPercent = $num * $percent / 100;
        return $numPercent;
    }

    /**
     * Определение цело или дробное
     * @param  [type] $sum [description]
     * @return [type]      [description]
     */
    public function myNum($sum){

        if ($sum > 0) {

            if (is_int($sum) && $sum > 0) {
                #echo "Целое!";
                
                if (is_numeric($sum)) {
                    #echo "Число!";
                    $theNumber=(filter_var($sum, FILTER_SANITIZE_NUMBER_INT));
                    $theNumber = (int)$theNumber;
                }else{
                    #echo "Не число!";
                    return false;
                }
            }else{
                #echo "Дробное!";
                
                if (is_numeric($sum)) {
                    #echo "Число!";
                    $theNumber=(filter_var($sum, FILTER_SANITIZE_NUMBER_FLOAT, array('flags' => FILTER_FLAG_ALLOW_FRACTION, FILTER_FLAG_ALLOW_THOUSAND)));
                }else{
                    #echo "Не число!";
                    return false;
                }
            }

        }else return false;

        return $theNumber;
    }

    public function error($res, $die=true)
    {
        echo "<pre>";
        var_dump($res);
        echo "</pre>";
        if ($die == true) {
            die();
        }
    }

    /* ===== CRON ======= */

    /* ===== / CRON ======= */

}

<?php

namespace application\classes;

use Exception;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Helpers\Emojify;
use Telegram\Bot\Objects\InputMedia;

class Core{

    private $time;
    private $conf;
    private $b;
    private $telegram;
    private $result;
    private static $group_id = 0;

    public function __construct($telegram, $result){
        $this->time = time();
        $this->conf = new configBot();
        $this->b    = new Bot();

        $this->telegram = $telegram;
        $this->result   = $result;
    }
    
    public function __destruct(){}

    public function Init()
    {
        // для определения дальнейшего выбора действий
        $rawData = json_decode($this->result, true);
        $this->router($rawData);
    }

    public function router($result){

        if ( isset($result['chat_join_request']) ) {
            $chat_id = $result['chat_join_request']['from']['id'];

            # Добавим в базу пользователя если еще нет
            $first_name = (isset($result["chat_join_request"]["from"]["first_name"])) ? $result["chat_join_request"]["from"]["first_name"] : 'no';
            $username = (isset($result["chat_join_request"]["from"]["username"])) ? $result["chat_join_request"]["from"]["username"] : 'no';
            $this->b->checkUser($chat_id, $this->telegram, $first_name, 'no', $username);

            $this->nowOpros($chat_id);
            die();
        }

        $first_name = (isset($result["message"]["from"]["first_name"])) ? $result["message"]["from"]["first_name"] : 'no';
        $last_name = (isset($result["message"]["from"]["last_name"])) ? $result["message"]["from"]["last_name"] : 'no';
        $username = (isset($result["message"]["from"]["username"])) ? $result["message"]["from"]["username"] : 'no';

        // берем технические данные id чата пользователя == его id и текст который пришел
        $chat_id = $this->getChatId($result);
        $message_id = $this->getMessageId($result);
        $text = $this->getText($result);
        $contact = $this->getContact($result);
        $location = $this->getLocation($result);

        $this->telegram->sendChatAction([ 'chat_id' => $chat_id, 'action' => 'typing', ]);
        
        // если пришли данные message
        if (array_key_exists("message", $result)) {

            # ВКЛ и ВЫКЛ бота
            while (true) {
                if ($this->b->isAdmin($chat_id) == true) {
                    break;
                }else{
                    # ВКЛ- и ВЫКЛючение БОТа
                    if ($this->b->getSettings()['on_off'] == 'off') {
                        $reply = $this->b->getSettings()['text_page'];

                        $this->telegram->sendMessage([ 'chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'Markdown', 'reply_markup' => Keyboard::remove([ 'remove_keyboard' => true, 'selective' => false ])
                        ]);
                        return;
                    }
                    break;
                }
            }
            # / --- END ---
            
            // текстовые данные
            if (array_key_exists("text", $result['message'])) {

                # отмена всех шагов
                $this->setCancel($chat_id, $message_id, $text);

                # проверка шагов, если есть обрабатываем
                //$this->getSteps($chat_id, $message_id, $text);

                switch ($text) {
                    //case '/start':
                    case mb_stripos($text,"/start") !== false OR
                         mb_stripos($text,"старт") !== false OR
                         mb_stripos($text,"start") !== false:
                        # Добавим в базу пользователя если еще нет
                        $this->b->checkUser($chat_id, $this->telegram, $first_name, $last_name, $username);

                        # Стартовое приветствие
                        $this->startBot($chat_id);

                    break;

                    case mb_stripos($text,"Назад") !== false:
                        # Стартовое приветствие
                        $this->startBot($chat_id);
                    break;

                    default;
                        /*$reply = "Нам пока не нужны эти данные. Спасибо!";
                
                        $this->telegram->sendMessage([ 'chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'Markdown', 'reply_markup' => Keyboard::make(['inline_keyboard' => [], 'resize_keyboard' => true, 'one_time_keyboard' => false ])
                        ]);*/
                    break;

                } # END CASE

            } elseif (array_key_exists("photo", $result['message'])) {
                // если пришли картинки то обрабатываем если ждем
            } else { // другие данные - документы стикеры аудио ...
                //$this->sendMessage($chat_id, "Нам пока не нужны эти данные. Спасибо.");
            }
          
        }// если пришел запрос на функцию обратного вызова
        elseif (array_key_exists("callback_query", $result)) {

            // Если нажали кнопку
            switch ($result['callback_query']['data']) {

                # cancel
                case mb_stripos($result['callback_query']['data'],"cancel") !== false:
                    # удалим все шаги
                    $this->b->dellStep($chat_id);
                    # Стартовое приветствие
                    $this->startBot($chat_id);
                break;

                # удаление сообщения
                case mb_stripos($result['callback_query']['data'],"delmess_") !== false:
                    $arr = explode("_", $result['callback_query']['data']);

                    # удалим сообщение
                    $this->delMes($chat_id, $message_id, 1);
                break;

                /* ======= АДМИНКА ========== */
                case mb_stripos($result['callback_query']['data'],"vkl") !== false OR mb_stripos($result['callback_query']['data'],"off") !== false:

                    $reply = ($result['callback_query']['data'] == 'vkl') ? '*Админ, бот ВКЛючен*' : '*Админ, бот ВЫКЛючен*';

                    $callback_data = ($result['callback_query']['data'] == 'vkl') ? 'off' : 'vkl';

                    $text = ($result['callback_query']['data'] == 'vkl') ? Emojify::text(':negative_squared_cross_mark:').' ВЫКЛючить?' : Emojify::text(':white_check_mark:').' ВКЛючить?';

                    if ($result['callback_query']['data'] == 'vkl') {
                        $this->b->updateSettings('on');
                    }elseif ($result['callback_query']['data'] == 'off') {
                        $this->b->updateSettings('off');
                    }else{
                        # ERROR!
                    }

                    $keyboard[] = [
                        Keyboard::inlineButton(['callback_data'=>$callback_data,'text'=>$text]),
                    ];

                    $this->telegram->editMessageText([ 'chat_id' => $chat_id, 'text' => $reply, 'message_id' => $message_id, 'parse_mode' => 'Markdown', 'reply_markup' => Keyboard::make(['inline_keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => true ])
                    ]);

                break;
                /* ======= END АДМИНКА ========== */

                # test
                /*case mb_stripos($result['callback_query']['data'],"test") !== false:
                    $res = $this->telegram->sendDice([ 'chat_id' => $chat_id, ]);  #  🏀  🎯   'emoji' => '🏀', 🎰 'emoji' => '🎰'
                    file_put_contents(DR."/log/test.txt", "\n===========\n\n".print_r($res, true)."\n", FILE_APPEND);
                break;*/

                # new
                case mb_stripos($result['callback_query']['data'],"new") !== false:

                break;

                default;
                break;

            } # END CASE
            
            # выводит что нажали
            if ($result['callback_query']['data'] == '1') {
                $text_show = "🕐 Error! Кто-то нажал не туда...\n\nПопробуйте ещё раз или отправьте боту /start\n\nСпасибо! 😊";
            }elseif( mb_stripos($result['callback_query']['data'],"iambuy_") !== false){
                $arr = explode("_", $result['callback_query']['data']);

                
            }elseif( mb_stripos($result['callback_query']['data'],"lang_") !== false){
                $arr = explode("_", $result['callback_query']['data']);
                

            }elseif( mb_stripos($result['callback_query']['data'],"buy_") !== false){
                $arr = explode("_", $result['callback_query']['data']);

                $text_show = "Успешно! 😊\nСсылка на оплату сгенерирована. После оплаты нажмите Я оплатил. ✅"; // 3  0  6

                
            }else{
                $text_show = '';
                #$text_show = $result['callback_query']['id'];
                #$text_show = $result['callback_query']['data'];
            }
            $params = [
                'callback_query_id'  => $result['callback_query']['id'],
                'text'               => $text_show, #"Пришел ответ: ".$result['callback_query']['data'], #$text_show,
                'show_alert'         => true,
                //'url'                => 'https://t.me/CryptoPlay_Bot?start=ass',
            ];
            $this->telegram->answerCallbackQuery($params);

        } // Здесь пришли пока не нужные нам форматы
        else {
            // вернем текст с ошибкой
            //$this->sendMessage($chat_id, "Нам пока не нужны эти данные. Спасибо.");
        }
        return true;
    }

    /*****************
    ----- CORE -------
    ******************/

    /**
     * Выводим приветственное слово
     * @param $chat_id
     */
    private function startBot($chat_id, $message_id = NULL)
    {
        //$reply = "*Добро пожаловать!*\n\nВыберите действие";
        $reply = $this->b->getText(1)['text_section'];
        //$reply = str_replace("{name_user}", $first_name, $text);

        $keyboard_main = Keyboards::actions()['keyboard_main'];

        if ( $message_id == NULL ) {

            # картинку отправим
            $this->telegram->sendPhoto([
                'chat_id'              => $chat_id,
                'photo'                => InputFile::create("images/head.png", "head.png"),
                'caption'              => $reply,
                'parse_mode'           => 'Markdown',
                'reply_markup'         => Keyboard::make(['keyboard' => $keyboard_main, 'resize_keyboard' => true, 'one_time_keyboard' => false ])
            ]);

            //file_put_contents(DR."/log/test.txt", "\n======".date("d.m.Y H:i:s", time())."=====\n\n".print_r($r, true)."\n", FILE_APPEND);

            /*$this->telegram->sendMessage([ 'chat_id' => $chat_id, 'text' => $reply, 'parse_mode' => 'HTML', 'reply_markup' => Keyboard::make(['keyboard' => $keyboard_main, 'resize_keyboard' => true, 'one_time_keyboard' => false ])
            ]);*/

        }else{
            $this->telegram->editMessageText([ 'chat_id' => $chat_id, 'text' => $reply, 'message_id' => $message_id, 'parse_mode' => 'Markdown', 'reply_markup' => Keyboard::make(['keyboard' => $keyboard_main, 'resize_keyboard' => true, 'one_time_keyboard' => false ])
            ]);
        }
    }

    private function setCancel($chat_id, $message_id, $text)
    {
        if ( mb_stripos($text, "Отмена") !== false ) {

            $this->b->dellStep($chat_id);
            $this->telegram->sendMessage([ 'chat_id' => $chat_id, 'text' => "✅ Отменено!", 'parse_mode' => 'Markdown', 'reply_markup' => Keyboard::make(['keyboard' => Keyboards::actions()['keyboard_main'], 'resize_keyboard' => true, 'one_time_keyboard' => true ])
            ]);

            return;
        }
    }

    private function getSteps($chat_id, $message_id, $text)
    {
        # Ждем ввода вопроса
        $res = $this->b->getStep($chat_id);
        if (isset($res) AND $res['type'] == 'question' AND $res['name_step'] == '1' AND $res['status'] == 0) {

        }else{
            /*$this->telegram->sendMessage([ 'chat_id' => $chat_id, 'text' => "Empty!", 'parse_mode' => 'Markdown', 'reply_markup' => Keyboard::make(['keyboard' => Keyboards::actions()['keyboard_main'], 'resize_keyboard' => true, 'one_time_keyboard' => true ])
            ]);*/
        }
    }

    private function myExplode($data, $type = "_")
    {
        return explode($type, $data);
    }

    private function sendTelegram($method, $response){
        $ch = curl_init('https://api.telegram.org/bot' . $this->conf->botTokens . '/' . $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);

        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
        curl_close($ch);

        if (isset($error_msg)) {
            return $error_msg;
        }else return $res;
    }

    # Стартовое приветствие + начать опрос
    private function nowOpros($chat_id){
        return true;
    }

    private function delMes($chat_id, $message_id, $num = 2)
    {
        /*try {
            // где-то глубоко внутри кода
            // соединение с базой данных
            $handler = mysqli_connect('localhost', 'root', '', 'test');*/
         
            try {
                if ($num == 2) {
                    # code...
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => $message_id ]);
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => ($message_id - 1) ]);
                }elseif ($num == 4) {
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => ($message_id + 1) ]);
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => $message_id ]);
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => ($message_id - 1) ]);
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => ($message_id - 2) ]);
                }else{
                    $this->telegram->deleteMessage([ 'chat_id' => $chat_id, 'message_id' => $message_id ]);
                }

                //throw new Exception('DB error');
            } catch (Exception $e) {
                // исключение поймали, обработали на своём уровне
                // и должны его пробросить вверх, для дальнейшей обработки
                throw new Exception('Catch exception', 0, $e);
                return false;
            } finally {
                // но, соединение с БД необходимо закрыть
                // будем делать это в блоке finally
                return true;
            }
         
            // этот код не будет выполнен, если произойдёт исключение в коде выше
            //echo "Ok";

        /*} catch (Exception $e) {
            // ловим исключение, и выводим текст
            echo $e->getMessage();
            echo "<br/>";
            // выводим информацию о первоначальном исключении
            echo $e->getPrevious()->getMessage();
        }*/
    }

    /** 
     * проверяем на админа
     * @param $chat_id
     * @return bool
     */
    private function isAdmin($chat_id)
    {
        // возвращаем true или fasle
        return $chat_id == $this->admin;
    }

    /** 
     * Получаем id чата
     * @param $data
     * @return mixed
     */
    private function getChatId($data)
    {
        if ($this->getType($data) == "callback_query") {
            return $data['callback_query']['message']['chat']['id'];
        }
        if ( isset($data['message']) ) {
            return $data['message']['chat']['id'];
        }
    }

    /**
     * Получаем локацию
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function getLocation($data)
    {
        if ($this->getType($data) == "callback_query") {
            if (isset($data['callback_query']['message']['location'])) {
                return $data['callback_query']['message']['location'];
            }
        }
        
        if (isset($data['message']['location'])) {
            return $data['message']['location'];
        }else return false;
    }

    /**
     * Получаем контакт
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function getContact($data)
    {
        if ($this->getType($data) == "callback_query") {
            if (isset($data['callback_query']['message']['contact']['phone_number'])) {
                return $data['callback_query']['message']['contact']['phone_number'];
            }
        }
        
        if (isset($data['message']['contact']['phone_number'])) {
            return $data['message']['contact']['phone_number'];
        }else return false;
    }

    /** 
     * Получаем type чата
     * @param $data
     * @return mixed
     */
    private function getChatType($data)
    {
        if ($this->getType($data) == "callback_query") {
            return $data['callback_query']['message']['chat']['type'];
        }
        return $data['message']['chat']['type'];
    }

    /** 
     * Получаем id сообщения
     * @param $data
     * @return mixed
     */
    private function getMessageId($data)
    {
        if ($this->getType($data) == "callback_query") {
            return $data['callback_query']['message']['message_id'];
        }
        if (isset($data['message'])) {
            return $data['message']['message_id'];
        }
    }

    /** 
     * получим значение текст
     * @return mixed
     */
    private function getText($data)
    {
        if ($this->getType($data) == "callback_query") {
            return $data['callback_query']['data'];
        }
        return isset($data['message']['text'])
                ? $data['message']['text']
                : false;
    }

    /** 
     * Узнаем какой тип данных пришел
     * @param $data
     * @return bool|string
     */
    private function getType($data)
    {
        if (isset($data['callback_query'])) {
            return "callback_query";
        } elseif (isset($data['message']['text'])) {
            return "message";
        } elseif (isset($data['message']['photo'])) {
            return "photo";
        } else {
            return false;
        }
    }
}
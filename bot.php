<?php
######################################
# Скрипт Telegram Bot
# E-mail: FreedomInTheDark@gmail.com
# Telegram: @FreedomInTheDark
######################################
header("HTTP/1.1 200 OK");
set_time_limit(0);
ignore_user_abort(true);

define("DR", $_SERVER['DOCUMENT_ROOT']);

# заккоментировать после отладки!!!
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

# === Логирование ошибок ===

# раскоментировать после отладки, ошибку будут в файле, а не экране
/*error_reporting(E_ALL);
ini_set('display_errors',0);
ini_set('log_errors',1);
set_error_handler("errorsLog");*/

/**
 * [errorsLog Логирование ошибок в *txt файл]
 * @param  [type] $errno  [description]
 * @param  [type] $errmsg [description]
 * @param  [type] $file   [description]
 * @param  [type] $line   [description]
 * @return [type]         [description]
 */
function errorsLog($errno, $errmsg, $file, $line)
{
    $fp=fopen(DR.'/log/logError.txt', 'a+');
    fwrite($fp, "errno: $errno\ntext: $errmsg\nfile: $file\nline: $line\n (".date("r").") \n===========================================\n\n");
    fclose($fp);
}

# === /Логирование ошибок ===

include(DR.'/vendor/autoload.php'); //Подключаем библиотеку

use application\classes\Core;
use application\classes\Database;
use application\classes\configBot;

use Telegram\Bot\Api;
use Telegram\Bot\Helpers\Emojify;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\Message;

$telegram = new Api(configBot::$botToken); //Устанавливаем токен, полученный у BotFather

$response = $telegram->getWebhookInfo();
file_put_contents(DR."/log/webhook.txt", "\n===========\n\n".$response."\n", FILE_APPEND);

if ( strlen(json_decode($response, true)['url']) >= 5 ) {
    # есть хук
}else{
    # нет хука
    $response = $telegram->setWebhook([
        'url' => configBot::$urlSite,
    ]);
    file_put_contents(DR."/log/setWebhook.txt", "\n====== setWebhook ======\n\n".$response."\n", FILE_APPEND);
}

$result = $telegram->getWebhookUpdates(); //Передаем в переменную $result полную информацию о сообщении пользователя
if (isset($result['update_id'])) {
    file_put_contents(DR."/log/textfile.txt", "\n=====".$result['update_id']."======\n\n".$result."\n", FILE_APPEND);

    $c = new Core($telegram, $result);
    $c->Init(); # стартуем
}

die('404');
